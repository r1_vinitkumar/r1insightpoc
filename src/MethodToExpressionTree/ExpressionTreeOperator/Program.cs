﻿using System;
using System.Linq.Expressions;
using MethodToExpressionTree;
namespace ExpressionTreeOperator
{
	class Program
	{

		static string methodText = @"public override object[] Interpret(string line, bool strictLineLength = true)
        {
            int cmc;
            if (!int.TryParse(line.Substring(1, 8).NullIfEmpty(), out cmc))
            {
                return null;
            }

            return new object[] {
                Filename.Substring(6, 4),
                cmc,
                line.Substring(10, 30).NullIfEmpty(),
                line.Substring(41, 30).NullIfEmpty(),
                row_number++,//line.Substring(72, 3).NullIfEmpty(),
				line.Substring(76, 10).NullIfEmpty(),
                line.Substring(90, 2).NullIfEmpty(),
                AuditIdentity
            };
        }";
		static void Main(string[] args)
		{
			ExpressionTreeManager expressionTreeManager = new ExpressionTreeManager();
			expressionTreeManager.methodText = methodText;
			string method = expressionTreeManager.InitializeExpressionTree();
			Console.WriteLine(method);

			//ExpressionTreeTest();

			//Expression.Lambda<Action>($method).Compile()();


			Console.ReadLine();
		}

		static void ExpressionTreeTest()
		{
			// Creating a parameter expression.  
			ParameterExpression value = Expression.Parameter(typeof(int), "value");

			// Creating an expression to hold a local variable.
			ParameterExpression result = Expression.Parameter(typeof(int), "result");

			// Creating a label to jump to from a loop.  
			LabelTarget label = Expression.Label(typeof(int));

			// Creating a method body.  
			BlockExpression block = Expression.Block(
				// Adding a local variable.  
				new[] { result },
				// Assigning a constant to a local variable: result = 1  
				Expression.Assign(result, Expression.Constant(1)),
					// Adding a loop.  
					Expression.Loop(
					   // Adding a conditional block into the loop.  
					   Expression.IfThenElse(
						   // Condition: value > 1  
						   Expression.GreaterThan(value, Expression.Constant(1)),
						   // If true: result *= value --  
						   Expression.MultiplyAssign(result,
							   Expression.PostDecrementAssign(value)),
						   // If false, exit the loop and go to the label.  
						   Expression.Break(label, result)
					   ),
				   // Label to jump to.  
				   label
				)
			);

			var method = Expression.Lambda<Func<int, int>>(block, value).Compile().ToString();
			var method1 = block.Expressions.ToString();
			// Compile and execute an expression tree.  
			int factorial = Expression.Lambda<Func<int, int>>(block, value).Compile()(5);

			Console.WriteLine(factorial);
			// Prints 120.
		}
	}
}
