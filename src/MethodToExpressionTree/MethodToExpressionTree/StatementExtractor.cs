﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MethodToExpressionTree
{
	public class VariableAttributes
	{
		public string DataType { get; set; }
		public string Name { get; set; }
		public string Value { get; set; }
	}
	public class StatementExtractor
	{
		List<string> dataTypes = new List<string>();		
		public StatementExtractor()
		{
			dataTypes.Add("int");
			dataTypes.Add("Int32");
			dataTypes.Add("string");
			dataTypes.Add("String");	
		}
		public List<VariableAttributes> GetVariables()
		{
			string varFinderString = RemoveStatementOnGivenIndex(GetDepthIndex(1, "{"), GetDepthIndexReverse(1, "}"));
			return VariableFinder(varFinderString);
		}
		public int CountMethodDepth()
		{
			int count = 0;
			int lengthCounter = -1;
			while (Method.text.Length > lengthCounter)
			{
				lengthCounter = Method.text.IndexOf("{", lengthCounter + 1);
				count++;
			}
			return count;
		}

		public int GetDepthIndex(int depth, string searchFor)
		{
			int count = 0;
			int lengthCounter = -1;
			while (Method.text.Length > lengthCounter)
			{
				lengthCounter = Method.text.IndexOf(searchFor, lengthCounter + 1);
				if (depth <= count)
				{
					break;
				}
				count++;
			}
			return lengthCounter;
		}

		public int GetDepthIndexReverse(int depth, string searchFor)
		{
			int count = 0;
			int lengthCounter = Method.text.Length;
			while (lengthCounter > 1)
			{
				lengthCounter = Method.text.LastIndexOf(searchFor, lengthCounter - 1);
				if (depth <= count)
				{
					break;
				}
				count++;
			}
			return lengthCounter;
		}

		public string RemoveStatementOnGivenIndex(int startIndex, int endIndex)
		{
			StringBuilder methodText = new StringBuilder(Method.text);
			methodText.Remove(startIndex, endIndex-startIndex);
			return methodText.ToString();
		}

		public string[] NewlineSpliter(string text)
		{
			string[] stringSeparators = new string[] { "\r\n" };
			return text.Split(stringSeparators, StringSplitOptions.None);			
		}

		public List<VariableAttributes> VariableFinder(string varFinderString)
		{
			List<VariableAttributes> variablelist = new List<VariableAttributes>();
			
			string[] statements = NewlineSpliter(varFinderString); 
			if (statements.Count() <= 0)
				return variablelist;
			
			foreach (string line in statements)
			{
				var statement = line.Trim();
				if (dataTypes.Any(x => statement.StartsWith(x)))
				{
					string[] statementBreakup = statement.Replace(";", "").Split(" ");
					string dataType = statementBreakup[0];
					if (statement.Contains(","))
					{
						for (int i = 1; i < statementBreakup.Length; i++)
						{
							variablelist.Add(new VariableAttributes { DataType = dataType, Name = statementBreakup[i].Replace(",","") });
						}
					}
					else
					{
						variablelist.Add(new VariableAttributes { DataType = dataType, Name = statementBreakup[1] });
					}
				}
			}
			return variablelist;
		}


		
	}
	public class Converter
	{
		delegate dynamic Action(string value);
		Dictionary<string, Action> converterList = new Dictionary<string, Action>();

		public static Dictionary<string, string> FormatTypes = new Dictionary<string, string>();
		 
		static Converter()
		{
			FormatTypes.Add("string", "String");
			FormatTypes.Add("int", "Int32");
			FormatTypes.Add("bool", "Boolean");
			FormatTypes.Add("decimal", "Decimal");			
		}
		public Converter()
		{
			converterList.Add("System.Boolean", ConvertToBool);
			converterList.Add("System.int", ConvertToInt);
			converterList.Add("System.String", ConvertToInt);
		}
		public dynamic ConvertTo(string type, string value)
		{
			var result = converterList[type](value);
			return result;
		}

		public dynamic ConvertToBool(string value)
		{
			return Convert.ToBoolean(value);
		}

		public dynamic ConvertToInt(string value)
		{
			return Convert.ToInt32(value);
		}

		public dynamic ConvertToString(string value)
		{
			return value.ToString();
		}
	}
}
