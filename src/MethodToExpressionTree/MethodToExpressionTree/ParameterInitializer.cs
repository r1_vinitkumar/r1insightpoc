﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MethodToExpressionTree
{
    public class ParameterInitializer
    {
		const string SystemName = "System";
		public ParameterInitializer()
		{

		}

		public List<ParameterExpression> GetMethodParameters()
		{
			string[] parametersString = Method.text.Split("(")[1].Split(")")[0].Split(",");
			List<ParameterExpression> parameterExpressions = new List<ParameterExpression>();
			if (parametersString.Count() <= 0)
				return parameterExpressions;
			
			foreach (string pramPair in parametersString)
			{
				string pair = pramPair.Trim();
				string typeName = SystemName + "." + Converter.FormatTypes[pair.Split(" ")[0]];
				Type type = Type.GetType(typeName);
				string parameterName;
				if (pair.Split(" ").Count() >2)
				{
					parameterName = pair.Split(" ")[1];
					string value = pair.Split(" ")[3];
					var parameter=Expression.Parameter(type, parameterName);
					Expression.Assign(parameter, Expression.Constant(new Converter().ConvertTo(typeName, value)));
					parameterExpressions.Add(parameter);					
				}
				else
				{
					parameterName = pair.Split(" ")[1];
					parameterExpressions.Add(Expression.Parameter(type, parameterName));
				}
			}
			return parameterExpressions;
		}
	}
}
