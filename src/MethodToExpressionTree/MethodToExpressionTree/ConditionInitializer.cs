﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MethodToExpressionTree
{
    public class ConditionInitializer
    {
		Dictionary<string, ParameterExpression> variables { get; set; }
		Dictionary<string, string> Opraters = new Dictionary<string, string>();
		public ConditionInitializer()
		{
			Opraters.Add("!", "Not");
		}
		//public Expression ExtractCondition()
		//{
		//}

    }

	public class ConditionAttributes
	{
		public ParameterExpression LeftVariable { get; set; }
		public ParameterExpression RightVariable { get; set; }
		public string ConditionalOperator { get; set; }		
	}
}
