﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MethodToExpressionTree
{
	public class StatementIdentifier
	{
		StatementExtractor statementExtractor;
		const string StartScope = "{";
		const string EndScope = "}";

		string[] methodText;
		public Dictionary<string, ParameterExpression> variables { get; set; }

		Dictionary<string, string> keyWords = new Dictionary<string, string>();
		List<string> dataTypes = new List<string>();
		public StatementIdentifier()
		{
			methodText = statementExtractor.NewlineSpliter(Method.text);

			keyWords.Add("if", "IfThen");
			keyWords.Add("ifelse", "IfThen");
			keyWords.Add("return", "Return");

			dataTypes.Add("for");
			dataTypes.Add("if");
		}

		public void GetIfthenElseExpression()
		{
			int countLines = 0;
			foreach (string lineFromMethod in methodText)
			{
				string line = lineFromMethod.Trim();
				if (dataTypes.Any(x => line.StartsWith(x)))
				{
				  var scopedStatement=GetScopedStatement(ref countLines, dataTypes.Where(x => line.StartsWith(x)).FirstOrDefault());
				}
				countLines++;
			}
		}

		public List<string> GetScopedStatement(ref int linenumber, string keyWord)
		{
			List<string> scopedStatement = new List<string>();
			int countLines;

			int scopeStartCount = 0;
			int scopeEndCount = 0;
			for (countLines = linenumber; linenumber < methodText.Length; countLines++)
			{
				if (countLines == linenumber)
				{
					scopedStatement.Add(methodText[countLines]);
					if (methodText[countLines].Trim().EndsWith(StartScope))
						scopeStartCount++;
				}
				if (methodText[countLines].Trim().StartsWith(StartScope))
				{
					scopedStatement.Add(methodText[countLines]);
					scopeStartCount++;
				}
				else if (methodText[countLines].Trim().EndsWith(EndScope))
				{
					scopedStatement.Add(methodText[countLines]);
					scopeEndCount++;
				}

				if (scopeStartCount == scopeEndCount)
				{
					linenumber = countLines;
					break;
				}
			}
			return scopedStatement;
		}


	}
}
