﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MethodToExpressionTree
{
	public class ExpressionTreeManager
	{
		public ExpressionTreeManager()
		{

		}

		public string methodText { get; set; }
		public string InitializeExpressionTree()
		{
			Method.text = methodText;
			ParameterInitializer parameterInitialiser = new ParameterInitializer();
			List<ParameterExpression> methodParameter = parameterInitialiser.GetMethodParameters();

			VariableInitializer variableInitializer = new VariableInitializer();
			List<ParameterExpression> methodVariables = variableInitializer.LocalVariableInitializer();

			SignatureInitializer signatureInitializer = new SignatureInitializer();
			signatureInitializer.MethodParameters = methodParameter;
			signatureInitializer.MethodVariables = methodVariables;

			BlockExpression block = signatureInitializer.InitialiseMethodSignature();
			var result = Expression.Lambda<Func<int>>(block).Compile();
			Expression convertExpr = Expression.Convert(block,typeof(string));
			var expression = GetDebugView(block);
			return expression;
		}

		public string GetDebugView(Expression exp)
		{
			if (exp == null)
				return null;
			var propertyInfo = typeof(Expression).GetProperty("DebugView", BindingFlags.Instance | BindingFlags.NonPublic);
			return propertyInfo.GetValue(exp) as string;
		}
	}
}
