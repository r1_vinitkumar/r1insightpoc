﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MethodToExpressionTree
{
	public class SignatureInitializer
	{
		public SignatureInitializer()
		{

		}

		public List<ParameterExpression> MethodParameters { get; set; }
		public List<ParameterExpression> MethodVariables { get; set; }

		Dictionary<string, ParameterExpression> variables = new Dictionary<string, ParameterExpression>();
		public BlockExpression InitialiseMethodSignature()
		{
			BlockExpression block;
			//var variables = new[] { MethodVariables.ToArray<object>()};
			MethodVariables.AddRange(MethodParameters);
			foreach (ParameterExpression pe in MethodVariables)
			{
				variables.Add(pe.Name, pe);
			}

			//if (MethodVariables.Count() <= 0)
			block = Expression.Block(
				MethodVariables.ToArray(),
				Expression.Assign(variables["cmc"], Expression.Constant(1))
				);
			//else
			//block = Expression.Block(
			// MethodVariables.ToArray()
			//);

			return block;
		}


	}
}
