﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MethodToExpressionTree
{
	public class VariableInitializer
	{
		const string SystemName = "System";
		public VariableInitializer()
		{

		}

		public List<ParameterExpression> LocalVariableInitializer()
		{
			StatementExtractor statementExtractor = new StatementExtractor();
			List<VariableAttributes> variableAttributesList = statementExtractor.GetVariables();

			List<ParameterExpression> localVariableList = new List<ParameterExpression>();
			if (variableAttributesList.Count() <= 0)
				return localVariableList;
			foreach (VariableAttributes variableAttributes in variableAttributesList)
			{
				string typeName = SystemName + "." + Converter.FormatTypes[variableAttributes.DataType];
				var parameter = Expression.Variable(Type.GetType(typeName), variableAttributes.Name);
				if (variableAttributes.Value != null)
				{
					Expression.Assign(parameter, Expression.Constant(
						new Converter().ConvertTo(typeName, variableAttributes.Value)));
				}
				localVariableList.Add(parameter);
			}
			return localVariableList;
		}
	}
}
